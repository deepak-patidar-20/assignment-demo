package com.assignment.demo.view

import android.nfc.tech.MifareUltralight.PAGE_SIZE
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.assignment.demo.R
import com.assignment.demo.model.ApiResponse
import com.assignment.demo.viewModel.MainViewModel
import kotlinx.android.synthetic.main.main_fragment.*


class MainFragment : Fragment() {

    var adapterItemList = ArrayList<ApiResponse>()
    var rvAdapter: MainAdapter? = null
    var layoutManager: LinearLayoutManager? = null
    var page: Int = 1
    var isLastPage = false
    var isLoading = false
    private val loadingItem = ApiResponse()

    companion object {
        fun newInstance(): MainFragment {
            return MainFragment()
        }
    }

    private var mViewModel: MainViewModel? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        System.out.println(" onCreateView:- $mViewModel")
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mViewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        rvAdapter = MainAdapter(adapterItemList)
        recyclerView.adapter = rvAdapter
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = layoutManager
        recyclerView.addOnScrollListener(recyclerViewOnScrollListener)

        progressBarAvatar.visibility = View.VISIBLE
        loadDataFromApi(page)
    }

    private fun loadDataFromApi(page: Int) {
        println("loadDataFromApi: $page")
        isLoading = true
        context?.let {
            mViewModel?.getResponse(it, page)?.observe(viewLifecycleOwner, responseObserver)
        }
    }

    private val recyclerViewOnScrollListener: RecyclerView.OnScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            val visibleItemCount = layoutManager?.childCount ?: 0
            val totalItemCount = layoutManager?.itemCount ?: 0
            val firstVisibleItemPosition = layoutManager?.findFirstVisibleItemPosition() ?: 0
            if (!isLoadMoreInProgress() && !isLastPage) {
                if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0 && totalItemCount >= PAGE_SIZE) {
                    addLoaderInList()
                    loadDataFromApi(page)
                }
            }
        }
    }

    private val responseObserver = Observer<List<ApiResponse>> { listResponse ->

        println("responseObserver: $page -- $listResponse")

        removeLoadMoreLoader()
        if (page == 1) {
            adapterItemList.clear()
        }
        if (!listResponse.isNullOrEmpty()) {
            adapterItemList.addAll(listResponse)
            rvAdapter?.notifyDataSetChanged()
            progressBarAvatar.visibility = View.GONE
            if (listResponse.size == 10) {
                page++
            } else {
                isLastPage = true
            }
        }
    }

    fun isLoadMoreInProgress(): Boolean {
        println("isLoadMoreInProgress: $page -- ${adapterItemList[adapterItemList.size - 1].showLoading}")
        return adapterItemList[adapterItemList.size - 1].showLoading
    }

    fun addLoaderInList() {
        if (!isLoadMoreInProgress()) {
            adapterItemList.add(loadingItem)
            rvAdapter?.notifyDataSetChanged()
        }
    }

    private fun removeLoadMoreLoader() {
        adapterItemList.removeAll { it.showLoading }
        if (adapterItemList.isNotEmpty()) {
            rvAdapter?.notifyDataSetChanged()
        }
    }

}