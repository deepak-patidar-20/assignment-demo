package com.assignment.demo.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.assignment.demo.R
import com.assignment.demo.model.ApiResponse


class MainAdapter(var adapterItemList: ArrayList<ApiResponse>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val viewTypeItem = 1
    private val viewTypeLoader = 2

    override fun getItemViewType(position: Int): Int {
        return when {
            adapterItemList[position].showLoading -> {
                viewTypeLoader
            }
            else -> {
                viewTypeItem
            }
        }
    }

    override fun getItemCount(): Int {
        return adapterItemList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return when (viewType) {
            viewTypeItem -> {
                ItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_item, parent, false))
            }
            else -> {
                ViewHolderLoaderItem(LayoutInflater.from(parent.context).inflate(R.layout.loader_item, parent, false))
            }
        }
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            viewTypeItem -> {
                (holder as ItemViewHolder).bind(position)
            }
            else -> {
                (holder as ViewHolderLoaderItem).bind(position)
            }
        }
    }

    inner class ViewHolderLoaderItem(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val pbLoader: TextView = itemView.findViewById(R.id.pbLoader)
        fun bind(position: Int) {
            pbLoader.visibility = View.VISIBLE
        }
    }

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val tvTitle: TextView = itemView.findViewById(R.id.title)
        private val tvDescription: TextView = itemView.findViewById(R.id.description)
        private val tvOpenIssues: TextView = itemView.findViewById(R.id.openIssues)
        private val cvLicense: ConstraintLayout = itemView.findViewById(R.id.cvLicense)
        private val license: TextView = itemView.findViewById(R.id.license)
        private val tvLicenseName: TextView = itemView.findViewById(R.id.name)
        private val tvLicenseUrl: TextView = itemView.findViewById(R.id.url)
        private val tvAdmin: TextView = itemView.findViewById(R.id.admin)
        private val tvPush: TextView = itemView.findViewById(R.id.push)
        private val tvPull: TextView = itemView.findViewById(R.id.pull)

        fun bind(position: Int) {
            val listItem = adapterItemList[position]

            tvTitle.text = listItem.name
            tvDescription.text = listItem.description
            tvOpenIssues.text = "${itemView.context.resources.getString(R.string.open_issues_label)}: ${listItem.openIssuesCount}"

            if (!listItem.license?.name.isNullOrEmpty()) {
                tvLicenseName.text = listItem.license?.name
                tvLicenseName.visibility = View.VISIBLE
            } else {
                tvLicenseName.visibility = View.GONE
            }

            if (!listItem.license?.url.isNullOrEmpty()) {
                tvLicenseUrl.text = listItem.license?.url
                tvLicenseUrl.visibility = View.VISIBLE
            } else {
                tvLicenseUrl.visibility = View.GONE
            }

            if (!listItem.license?.name.isNullOrEmpty() && !listItem.license?.url.isNullOrEmpty()) {
                license.visibility = View.VISIBLE
                cvLicense.visibility = View.VISIBLE
            } else {
                license.visibility = View.GONE
                cvLicense.visibility = View.GONE
            }

            tvAdmin.text = "Admin: ${listItem.permissions?.admin}"
            tvPush.text = "Push: ${listItem.permissions?.push}"
            tvPull.text = "Pull: ${listItem.permissions?.pull}"
        }
    }
}