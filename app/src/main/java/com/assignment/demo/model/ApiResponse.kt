package com.assignment.demo.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class ApiResponse : RealmObject() {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    var id: String? = null

    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("full_name")
    @Expose
    var fullName: String? = null

    @SerializedName("description")
    @Expose
    var description: String? = null

    @SerializedName("open_issues_count")
    @Expose
    var openIssuesCount: Int? = null

    @SerializedName("license")
    @Expose
    var license: License? = null

    @SerializedName("permissions")
    @Expose
    var permissions: Permissions? = null

    var showLoading = false

}