package com.assignment.demo.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class Permissions : RealmObject() {

    @SerializedName("admin")
    @Expose
    var admin: Boolean? = null

    @SerializedName("push")
    @Expose
    var push: Boolean? = null

    @SerializedName("pull")
    @Expose
    var pull: Boolean? = null
}