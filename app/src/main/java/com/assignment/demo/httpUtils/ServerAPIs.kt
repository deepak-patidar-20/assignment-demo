package com.assignment.demo.httpUtils

import com.assignment.demo.model.ApiResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ServerAPIs {

    @GET("/orgs/octokit/repos?per_page=10")
    fun getApiResponse(@Query("page") page: Int): Call<List<ApiResponse>>

}