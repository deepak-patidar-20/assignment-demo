package com.assignment.demo.utils

import android.content.Context
import android.net.ConnectivityManager

object BaseUtils {

    fun checkConnection(context: Context?): Boolean {
        val cm = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager
        return cm?.activeNetwork != null
    }

}