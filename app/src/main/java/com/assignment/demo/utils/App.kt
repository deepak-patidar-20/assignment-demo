package com.assignment.demo.utils

import android.app.Application
import android.content.Context
import com.assignment.demo.dbUtils.DBHelper

class App: Application() {

    override fun onCreate() {
        super.onCreate()

        DBHelper(applicationContext)

    }
}