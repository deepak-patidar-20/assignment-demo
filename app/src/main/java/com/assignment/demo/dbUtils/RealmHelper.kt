package com.assignment.demo.dbUtils

import android.content.Context
import io.realm.Realm
import io.realm.RealmConfiguration

object RealmHelper {

    fun getRealm(context: Context): Realm{

        val mRealm: Realm?

        Realm.init(context)
        val config = RealmConfiguration
                .Builder()
                //.schemaVersion(0) // Must be bumped when the schema changes
                //.migration(MyRealmMigration()) // Migration to run
                .deleteRealmIfMigrationNeeded()
                .build()

        mRealm = Realm.getInstance(config)

        return mRealm

    }
}