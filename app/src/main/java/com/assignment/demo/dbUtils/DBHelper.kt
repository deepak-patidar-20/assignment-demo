package com.assignment.demo.dbUtils

import android.content.Context
import com.assignment.demo.model.ApiResponse
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.RealmObject
import io.realm.exceptions.RealmException

object DBHelper {

    private var mRealm: Realm? = null

    operator fun invoke(context: Context): Realm? {

        println("DBHelper invoke $context")

        Realm.init(context)
        val config = RealmConfiguration
                .Builder()
                //.schemaVersion(0) // Must be bumped when the schema changes
                //.migration(MyRealmMigration()) // Migration to run
                .deleteRealmIfMigrationNeeded()
                .build()

        mRealm = Realm.getInstance(config)

        return mRealm
    }

    fun getApiResponseList() = try {

        println("getApiResponseList mRealm $mRealm")

        mRealm?.where(ApiResponse::class.java)?.findAll()
    } catch (e: Exception) {
        println("getApiResponseList Exception ${e.localizedMessage}")
        null
    }

    fun saveArrayToDBAsync(list: ArrayList<ApiResponse>?) {

        println("saveArrayToDBAsync mRealm $mRealm")

        list?.let { tempList ->
            try {
                mRealm?.executeTransactionAsync({
                    for (item in tempList) {
                        it.copyToRealmOrUpdate(item as RealmObject)
                    }
                }, {
                    println("saveArrayToDBAsync ${list} was added")
                }, {
                    println("saveArrayToDBAsync throwable ${it.message}")
                })
            } catch (re: RealmException) {
                println("saveArrayToDBAsync realmException ${re.message}")
            } catch (e: Exception) {
                println("saveArrayToDBAsync Exception ${e.message}")
            }
        }
    }

    fun deleteAllResponseFromDB() {
        try {
            mRealm?.executeTransaction {
                val result = mRealm?.where(ApiResponse::class.java)?.findAll()
                result?.deleteAllFromRealm()
            }
        } catch (e: Exception) {
            println("Exception deleteAllFeatures ${e.message}")
        }
    }
}