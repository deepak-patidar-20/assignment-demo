package com.assignment.demo.viewModel

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.assignment.demo.dbUtils.DBHelper
import com.assignment.demo.model.ApiResponse
import com.assignment.demo.repository.Repository
import com.assignment.demo.utils.BaseUtils

class MainViewModel : ViewModel() {

    var apiResponseLiveData = MutableLiveData<List<ApiResponse>>()

    fun getResponse(context: Context, page: Int): MutableLiveData<List<ApiResponse>> {
        if (!BaseUtils.checkConnection(context)) {
            Toast.makeText(context, "No Internet connection!", Toast.LENGTH_SHORT).show()
            apiResponseLiveData.value = DBHelper.getApiResponseList() as List<ApiResponse>
        } else {
            apiResponseLiveData = Repository.getResponseFromAPI(page)
        }

        return apiResponseLiveData
    }

}