package com.assignment.demo.repository

import androidx.lifecycle.MutableLiveData
import com.assignment.demo.dbUtils.DBHelper
import com.assignment.demo.httpUtils.APIClient
import com.assignment.demo.model.ApiResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object Repository {

    fun getResponseFromAPI(page: Int): MutableLiveData<List<ApiResponse>> {

        val mutableLiveData = MutableLiveData<List<ApiResponse>>()
        val serverAPIs = APIClient.getClient()

        println("getResponseFromAPI: $page")

        val call: Call<List<ApiResponse>> = serverAPIs.getApiResponse(page)
        call.enqueue(object : Callback<List<ApiResponse>> {
            override fun onResponse(call: Call<List<ApiResponse>>?, response: Response<List<ApiResponse>>?) {
                mutableLiveData.value = response?.body()
                if (page == 1) {
                    DBHelper.deleteAllResponseFromDB()
                }
                if (!response?.body().isNullOrEmpty()) {
                    DBHelper.saveArrayToDBAsync(response?.body() as ArrayList<ApiResponse>)
                }
            }

            override fun onFailure(call: Call<List<ApiResponse>>?, t: Throwable?) {

            }
        })

        return mutableLiveData
    }

}